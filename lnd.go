package main

import (
	"context"
	"encoding/hex"
	"fmt"
	"io/ioutil"

	"github.com/lightningnetwork/lnd/lnrpc"
	"github.com/lightningnetwork/lnd/macaroons"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"gopkg.in/macaroon.v2"
)

func (l *lnd) Connect(
	nodeUrl string,
	macaroonPath string,
	certPath string,
) {
	tlsCreds, err := credentials.NewClientTLSFromFile(certPath, "")
	if err != nil {
		fmt.Println("Cannot get node tls credentials", err)
		return
	}

	macaroonBytes, err := ioutil.ReadFile(macaroonPath)
	if err != nil {
		fmt.Println("Cannot read macaroon file", err)
		return
	}

	mac := &macaroon.Macaroon{}
	if err = mac.UnmarshalBinary(macaroonBytes); err != nil {
		fmt.Println("Cannot unmarshal macaroon", err)
		return
	}
	//#endregion

	//#region dial grpc
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(tlsCreds),
		grpc.WithBlock(),
		grpc.WithPerRPCCredentials(macaroons.NewMacaroonCredential(mac)),
	}

	conn, err := grpc.Dial(nodeUrl, opts...)
	if err != nil {
		fmt.Println("cannot dial to lnd", err)
		return
	}

	l.lndClient = lnrpc.NewLightningClient(conn)
	//spew.Dump(l.lndClient)
	//#endregion
}

func (l *lnd) GetUri() string {
	getInfoReq := lnrpc.GetInfoRequest{}
	ctx := context.Background()
	res, err := l.lndClient.GetInfo(ctx, &getInfoReq)
	if err != nil {
		fmt.Println(err)
	}
	//spew.Dump(res)
	return res.GetUris()[0]
}

func (l *lnd) IsPeer(pubkey string) bool {
	peersReq := lnrpc.ListPeersRequest{}
	ctx := context.Background()
	peersRes, err := l.lndClient.ListPeers(ctx, &peersReq)
	if err != nil {
		fmt.Println(err)
		return false
	}
	for _, k := range peersRes.Peers {
		//fmt.Println(k.PubKey)
		if k.PubKey == pubkey {
			return true
		}
	}
	return false
}

func (l *lnd) OpenChannel(pubkey string, capacity, pushAmt int64, private bool) error {
	b, err := hex.DecodeString(pubkey)
	if err != nil {
		return credentials.ErrConnDispatched
	}
	//spew.Dump(b)
	params := lnrpc.OpenChannelRequest{
		NodePubkey:         b,
		LocalFundingAmount: capacity,
		PushSat:            pushAmt,
		Private:            private,
	}
	//spew.Dump(params)
	ctx := context.Background()
	res, err := l.lndClient.OpenChannel(ctx, &params)
	for {
		_, err := res.Recv()
		//fmt.Println(up)
		if err != nil {
			fmt.Println("LNURL : Error opening channel with: " + pubkey)
			return err
		}
		fmt.Println("LNURL : Done open channel with: " + pubkey)
		return nil
	}
	//err = res.RecvMsg(v)

}

func (l *lnd) HasChannelWith(pubkey string) bool {
	ctx := context.Background()
	listChans, err := l.lndClient.ListChannels(ctx, &lnrpc.ListChannelsRequest{})
	if err != nil {
		fmt.Println(err)
		return true
	}
	chansArr := listChans.GetChannels()
	//spew.Dump(chansArr)
	for _, k := range chansArr {
		remotePub := k.GetRemotePubkey()
		if remotePub == pubkey {
			return true
		}
	}

	pendingChans, err := l.lndClient.PendingChannels(ctx, &lnrpc.PendingChannelsRequest{})
	if err != nil {
		fmt.Println(err)
		return true
	}
	pendArr := pendingChans.GetPendingOpenChannels()
	//spew.Dump(pendArr)
	for _, k := range pendArr {

		pendingChan := k.GetChannel()
		remotePub := pendingChan.GetRemoteNodePub()
		if remotePub == pubkey {
			return true
		}
	}
	return false
}
