package main

import (
	"github.com/btcsuite/btcutil/bech32"
)

func (l *lnurl) Encode(data string) (string, error) {
	dataB := []byte(data)
	// Convert test data to base32:
	conv, err := bech32.ConvertBits(dataB, 8, 5, true)
	if err != nil {
		return "", err
	}
	return bech32.Encode(l.tag, conv)

}
