package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"encoding/json"

	"github.com/gorilla/mux"
)

type ServerInfo struct {
	key  string
	cert string
}

func main() {
	config := GetSettings("settings.json")
	l := lnd{}
	l.Connect(config.LndAddress, config.MacaroonPath, config.CertPath)
	g := gun{}
	go g.StartGun(config.GunAddress, config.GunPeer)

	lu := lnurl{
		tag: "lnurl",
	}
	ll, err := lu.Encode(config.Protocol + config.Domain + config.CallbackPort + "/request-channel")
	if err != nil {
		fmt.Println(err)
	}

	encoded := strings.ToUpper(ll)
	fmt.Println("URL - " + config.Protocol + config.Domain + config.CallbackPort + "/request-channel")
	fmt.Println("LNURL - ", encoded)

	s := server{
		gun:    &g,
		config: config,
		lnd:    &l,
		lnul:   lu,
	}
	r := mux.NewRouter()
	r.HandleFunc("/request-channel", s.RequestChannel)
	r.HandleFunc("/get-channel", s.GetChannel)
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(config.Port, nil))
}

func GetSettings(filePath string) serverConfig {
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	sc := serverConfig{}
	err = fromJsonB(b, &sc)
	if err != nil {
		fmt.Println(err)
	}
	//spew.Dump(sc)
	/*err = mapstructure.Decode(jsonMap, &sc)
	if err != nil {
		fmt.Println(err)
	}*/
	return sc
}

func sendError(w http.ResponseWriter, reason string) {
	errorMex := make(map[string]interface{})
	errorMex["status"] = "ERROR"
	errorMex["reason"] = reason
	b, err := toJsonS(errorMex)
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Println(b)
	fmt.Fprintf(w, b)
}

func sendOk(w http.ResponseWriter) {
	okMex := make(map[string]interface{})
	okMex["status"] = "OK"
	b, err := toJsonS(okMex)
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Println(b)
	fmt.Fprintf(w, b)
}

func fromJsonB(b []byte, v interface{}) error {
	err := json.Unmarshal(b, &v)
	return err
}
func fromJsonS(s string, v interface{}) error {
	err := fromJsonB(([]byte)(s), v)
	return err
}

func toJsonB(v interface{}) ([]byte, error) {
	return json.MarshalIndent(v, "", "   ")
}
func toJsonS(v interface{}) (string, error) {
	b, err := toJsonB(v)
	//spew.Dump(b)
	return string(b), err
}
