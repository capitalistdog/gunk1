package main

import (
	"fmt"
	"net/http"
	"strconv"
)

func (s *server) RequestChannel(w http.ResponseWriter, req *http.Request) {
	vars := req.URL.Query()
	session := vars.Get("session")
	//spew.Dump(vars)
	//r := NewRequestChannelResponse("uri", "http://localhost:8080/get-channel?meta=123456", "k1", "tag")
	uri := s.lnd.GetUri()
	r := requestChannelResponse{
		URI:      uri,
		Callback: s.config.Protocol + s.config.Domain + s.config.CallbackPort + "/get-channel",
		K1:       "gun",
		Tag:      "channelRequest",
	}
	fmt.Fprintf(w, r.getJSON())
	sess, err := strconv.Atoi(session)
	if err != nil {
		fmt.Println("LNURL :/request-channel Params sent correctly")
		return
	}

	fmt.Println("LNURL session=" + strconv.Itoa(sess) + " :/request-channel Params sent correctly")

}

func (r *requestChannelResponse) getJSON() string {

	s, err := toJsonS(r)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	return s

}
