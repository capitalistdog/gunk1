const getGunName = async (gun,key) =>{
    console.log(key)
    return await gun.user(key).get('Profile').get('displayName').then()
}
var Gun = require('gun');
console.log("running with peer:",process.argv[2])
var gun = new Gun({axe:false,peers:[process.argv[2]]});
const verifyName = async (req,res) =>{
    try{
        name = await getGunName(gun,req.query.shockid)
        if(name){
            console.log("Handling user:"+name)
            res.send({status:"OK",name})
        } else {
            throw new Error("Display name not found")
        }
    } catch (e){
        res.send({status:"ERROR",reason:e})
    }
    
}



const express = require('express')
const app = express()
const port = 3000

app.get('/verifyName',verifyName )

app.listen(port, () => console.log(`Gun listening on port ${port}!`))