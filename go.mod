module main

go 1.13

require (
	github.com/btcsuite/btcutil v0.0.0-20190425235716-9e5f4b9a998d
	github.com/davecgh/go-spew v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/lightningnetwork/lnd v0.9.0-beta
	github.com/mdp/qrterminal/v3 v3.0.0
	github.com/mitchellh/mapstructure v1.1.2
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	google.golang.org/grpc v1.26.0
	gopkg.in/macaroon.v2 v2.0.0
)
