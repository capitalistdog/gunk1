package main

import (
	"fmt"
	"net/http"
	"strconv"

	_ "github.com/gorilla/mux"
)

func (s *server) GetChannel(w http.ResponseWriter, req *http.Request) {

	vars := req.URL.Query()
	k1 := vars.Get("k1")
	remoteID := vars.Get("remoteid")
	if k1 == "" || remoteID == "" {
		fmt.Println("missing params k1 or remoteid")
		sendError(w, "missing params k1 or remoteid")
		return
	}
	private := vars.Get("private")
	prInt, err := strconv.Atoi(private)
	priv := false
	if prInt != 0 && prInt != 1 {
		fmt.Println("unknown private field in /get-channel request")
		sendError(w, "unknown private field")
		return
	}
	if prInt == 1 {
		priv = true
	}
	if err != nil {
		fmt.Println(err)
		sendError(w, "unknown private field")
		return
	}

	//spew.Dump(vars)
	nameRespS, err := s.gun.GetDisplayName(k1)
	if err != nil {
		fmt.Println(err)
		sendError(w, "internal error")
		return
	}
	nameResp := getDisplayNameResponse{}
	err = fromJsonS(nameRespS, &nameResp)
	if err != nil {
		fmt.Println(err)
		sendError(w, "internal error")
		return
	}

	if nameResp.Status != "OK" {
		sendError(w, "error, cant find display name")
		return
	}
	if s.lnd.HasChannelWith(remoteID) {
		sendError(w, "channel already exists")
		return
	}
	if !s.lnd.IsPeer(remoteID) {
		sendError(w, "not a peer")
		return
	}

	err = s.lnd.OpenChannel(remoteID, s.config.DefCapcity, s.config.DefPush, priv)
	if err != nil {
		fmt.Println(err)
		sendError(w, "cant open channel")
		return
	}

	sendOk(w)
}
