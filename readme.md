

## Settings



    
"lnd_proto_path":"private/rpc.proto", //not necessary

"lnd_cert_path":"private/tls.cert", //necessary not present by default

"lnd_macaroon_path":"private/admin.macaroon", //necessary not present by default

"lnd_node_address":"localhost:10009", //necessary

"gun_node_address":"http://localhost:3000", //don't touch this unless you have an external server to check gun display name

"default_channel_capacity":100000, //necessary

"default_push_amount":1000, //necessary

"domain":"192.168.0.20", //necessary: the domain for the LNURL to be generated and for the callback

"port":":8080", //necessary: the port LNURL server will listen on locally

"callback_port":":8080", //not necessary: the port to add to the LNURL and callback in case != ":80"

"protocol":"http://" //necessary: the protocol to use to generate the LNURL and callback prod -> https://

"gun_peer":"http://gun.shock.network:8765/gun" //necessary: the peer gun should connect to



## Build and run

### With docker

It's important for the LND certificate and macaroon to be in the folder /private if building with Docker, the names of files can be defined in the settings

- docker build -t example_name .
- docker run -d -p 8080:8080 example_name

### Without docker

- go build -o main .
- cd js
- npm install
- cd ..
- ./main