package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os/exec"
	"strings"
)

func (g *gun) StartGun(address string, peer string) error {
	g.deamonAddress = address
	cmd := exec.Command("node", "js/main.js", peer)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		panic(err)
	}
	err = cmd.Start()
	if err != nil {
		panic(err)
	}
	go copyOutput(stdout)
	go copyOutput(stderr)
	cmd.Wait()
	fmt.Println("Gun has stopped")
	return errors.New("Gun has stopped")
}

func copyOutput(r io.Reader) {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func (g *gun) GetDisplayName(shockID string) (string, error) {
	//key := "$$__SHOCKWALLET__USER__4u09Acy9TSZlwokgkATfy76ZBpr2HFqJIjeKIS7Q4mw.jKYs7MZW_PmP-m2dWFrlg4QQNFQrcOaoWIImWQwTDq4"
	splitted := strings.Split(shockID, "$$__SHOCKWALLET__USER__")
	//fmt.Println(g.deamonAddress)
	resp, err := http.Get(g.deamonAddress + "/verifyName?shockid=" + splitted[1])
	if err != nil {
		return "", err
	}
	body, err := ioutil.ReadAll(resp.Body)
	return string(body), nil
}
