package main

import (
	"github.com/lightningnetwork/lnd/lnrpc"
)

type Server interface {
	Start(configPath string)
}

type server struct {
	lnd    *lnd
	gun    *gun
	config serverConfig
	lnul   lnurl
}
type serverConfig struct {
	ProtoPath    string `json:"lnd_proto_path" `
	CertPath     string `json:"lnd_cert_path" `
	MacaroonPath string `json:"lnd_macaroon_path" `
	LndAddress   string `json:"lnd_node_address" `
	GunAddress   string `json:"gun_node_address" `
	DefCapcity   int64  `json:"default_channel_capacity" `
	DefPush      int64  `json:"default_push_amount" `
	Domain       string `json:"domain" `
	Port         string `json:"port" `
	CallbackPort string `json:"callback_port" `
	Protocol     string `json:"protocol" `
	GunPeer      string `json:"gun_peer" `
}

type LNURL interface {
	Encode(data string) (string, error)
}

type lnurl struct {
	tag string
}

type httpResponse interface {
	getJSON() string
}

type getDisplayNameResponse struct {
	Status string `json:"status" `
}

type requestChannelResponse struct {
	URI      string `json:"uri" `
	Callback string `json:"callback" `
	K1       string `json:"k1" `
	Tag      string `json:"tag" `
}

type getChannelResponse struct {
	Status string `json:"status" `
	Reason string `json:"reason" `
}

type LND interface {
	Connect(
		url string,
		macaroonPath string,
		certPath string,
		protoPath string,
	) error
	GetUri() string
	OpenChannel(pubkey string, capacity, pushAmt int64, private bool) error
	IsPeer(pubkey string) bool
	HasChannelWith(pubkey string) bool
}

type lnd struct {
	lndClient lnrpc.LightningClient
}

type GUN interface {
	startGun() error
}

type gun struct {
	deamonAddress string
}
